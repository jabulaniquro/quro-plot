import { DynamoDB, config } from 'aws-sdk'
import path from 'path'
import pltly from 'plotly'
const plotly = pltly("kamikazeovrld", "RWsG5raQKbfBd12PQP8p")

config.update({ region: 'us-east-1' })
const ddb = new DynamoDB({ apiVersion: '2012-08-10' })

const patchId = 'VC3ZA4D1Q_8NE3FK'

//joining path of directory 
const directoryPath = path.join(__dirname, 'data')
const filePath = path.join(directoryPath, 'cache.json')

const expectedReadings: { [key: string]: number } = {
    "ecg": 112500,
    "htr": 225,
    "rsr": 225,
    "stp": 225,
    "rri": 225,
    "sts": 900,
    "pos": 900
}

const toDoubleDigit: (str: string) => string = (str: string) => str.length == 2 ? str : toDoubleDigit(`0${str}`)
const toFourDigit: (str: string) => string = (str: string) => str.length == 4 ? str : toFourDigit(`0${str}`)
const formatTime = (epoch: number) => {
    console.log(`epoch - ${epoch}`)
    const date = new Date(epoch)
    console.log(`year`)
    const year = date.getFullYear()
    console.log(`month ${date.getMonth() + 1}`)
    const month = toDoubleDigit(`${date.getMonth() + 1}`)
    console.log(`day`)
    let day = toDoubleDigit(`${date.getDate()}`)
    console.log(`hour`)
    let hour = toDoubleDigit(`${date.getHours()}`)
    console.log(`min`)
    let min = toDoubleDigit(`${date.getMinutes()}`)
    console.log(`sec`)
    let sec = toDoubleDigit(`${date.getSeconds()}`)
    let millis = toFourDigit(`${date.getMilliseconds()}`)
    // let mi = toDoubleDigit(`${ date.getSeconds() }`)
    console.log(`millis = ${date.getMilliseconds()} / ${millis}`)
    return `${year} - ${month} - ${day} ${hour}: ${min}: ${sec}: ${millis}`
}

const getTimePeriodForTime: (startTime: number) => number = (startTime) => {
    const date = new Date(startTime)
    const minutes = (Math.floor((date.getMinutes() + 15) / 15) * 15) % 60
    const hours = date.getHours() + minutes == 0 ? 1 : 0
    date.setHours(hours, minutes, 0, 0)
    return date.getTime()
}


type TypeRawPlot = {
    [key: string]: {
        x: number[],
        y: number[]
    }
}
const fetchRawPlot: () => Promise<TypeRawPlot> = () => {
    const params = {
        ExpressionAttributeNames: {
            "#pk": "pk",
            "#sk": "sk"
        },
        ExpressionAttributeValues: {
            ":pk": {
                S: patchId
            },
            ":prefix": {
                S: 'rdd#'
            }
        },
        KeyConditionExpression: "#pk = :pk and begins_with(#sk, :prefix)",
        TableName: 'data'
    }
    console.log(`params = ${JSON.stringify(params)}`)
    process.exit()

    return new Promise<TypeRawPlot>((resolve, reject) => {
        ddb.query(params, (err, data) => {
            if (err) {
                reject(err)
                console.log("E:", err)
                console.log(data)
                process.exit()
            } else if (data && data.Items) {
                console.log(JSON.stringify(data))
                const result: any = {}
                // const x = []
                // const y = []
                data.Items.map((item) => {
                    console.log(JSON.stringify(item))
                    const { pk: { S: pk }, sk: { S: sk }, dt: { S: dt } } = item
                    const prefix = sk.substr(4, 3)
                    console.log(prefix)
                    if (!result[prefix]) {
                        result[prefix] = {
                            x: [],
                            y: []
                        }
                    }
                    result[prefix].x.push(parseInt(sk.substr(8)))
                    result[prefix].y.push(dt)

                })
                console.log(JSON.stringify(result))
                resolve(result)
            }
        })
    })
}
/*
const fetchRawPlotRetro: () => Promise<TypeRawPlot> = () => {
    return new Promise((resolve, reject) => {
        const params = {
            ExpressionAttributeNames: {
                "#pk": "pk",
                "#sk": "sk"
            },
            ExpressionAttributeValues: {
                ":pk": {
                    S: "PatchEntry"
                },
                ":sk": {
                    S: patchId
                }
            },
            KeyConditionExpression: "#pk = :pk and #sk = :sk",
            TableName: 'data'
        }


        ddb.query(params, (err, data) => {
            if (err) {
                return reject(err)
            } else if (data.Items) {
                console.log(JSON.stringify(data))
                if (data.Items.length != 1) {
                    return reject("ERR incorrect results")
                }
                const startTime: number = parseInt(data.Items[0].dt.N || '0')
                console.log(startTime)
                resolve(startTime)
            } else {
                return reject("ERR no data")
            }
        })
    })
        .then((startTime) => {
            let currentPeriod = getTimePeriodForTime(startTime)
            const now = Date.now()
            const periods: number[] = []

            while (currentPeriod <= now) {
                periods.push(currentPeriod)
                currentPeriod += 15 * 60 * 1000
            }

            const groups: number[][] = periods.reduce((acc, currentPeriod) => {
                if (acc[acc.length - 1].length >= 5) {
                    acc.push([])
                }
                acc[acc.length - 1].push(currentPeriod)
                return acc
            }, [[]])

            const results: any[] = []

            groups.reduce((promise, group) => {
                const promises = group.map((currentPeriod) => {
                    const params = {
                        ExpressionAttributeNames: {
                            "#pk": "pk",
                            "#sk": "sk"
                        },
                        ExpressionAttributeValues: {
                            ":pk": {
                                S: patchId
                            },
                            ": ": {
                                S: patchId
                            }
                        },
                        KeyConditionExpression: "#pk = :pk and #sk = :sk",
                        TableName: 'data'
                    }

                })
            }, Promise.resolve())
            


        })
    const params = {
        ExpressionAttributeNames: {
            "#pk": "pk",
            "#sk": "sk"
        },
        ExpressionAttributeValues: {
            ":pk": {
                S: patchId
            },
            ":prefix": {
                S: 'rdd#'
            }
        },
        KeyConditionExpression: "#pk = :pk and begins_with(#sk, :prefix)",
        TableName: 'data'
    }

    return new Promise<TypeRawPlot>((resolve, reject) => {
        ddb.query(params, (err, data) => {
            if (err) {
                reject(err)
                console.log("E:", err)
                console.log(data)
                process.exit()
            } else if (data && data.Items) {
                console.log(JSON.stringify(data))
                const result: any = {}
                // const x = []
                // const y = []
                data.Items.map((item) => {
                    console.log(JSON.stringify(item))
                    const { pk: { S: pk }, sk: { S: sk }, dt: { S: dt } } = item
                    const prefix = sk.substr(4, 3)
                    console.log(prefix)
                    if (!result[prefix]) {
                        result[prefix] = {
                            x: [],
                            y: []
                        }
                    }
                    result[prefix].x.push(parseInt(sk.substr(8)))
                    result[prefix].y.push(dt)

                })
                console.log(JSON.stringify(result))
                resolve(result)
            }
        })
    })
}
*/

const plotRawPlot: (data: TypeRawPlot) => void = (data) => {
    const traces: any[] = []
    for (const prefix in expectedReadings) {
        if (data.hasOwnProperty(prefix)) {
            const x = data[prefix].x.map((value) => {
                return formatTime(value)
            })
            /*traces.push({
                ...data[prefix],
                x,
                type: 'scatter',
                name: prefix,
                xaxis: 'time',
                yaxis: prefix
            })*/
            const y = data[prefix].y.map((value) => {
                console.log(`${prefix} - ${value}`)
                return Math.floor(((value / expectedReadings[prefix]) * 100) * 100) / 100
            })
            traces.push({
                x,
                y,
                type: 'scatter',
                name: `${prefix} percentage`,
                xaxis: 'time',
                yaxis: `${prefix} % `
            })
        } else {
            console.log(`NOT FOUND > ${prefix}`)
        }
    }
    const layout = {
        grid: { rows: 10, columns: 2, pattern: 'independent' },
    }
    plotly.plot(traces, layout, (err: any, msg: any) => {
        if (err) return console.log(err);
        console.log(msg);
    })
    /*for (const prefix in data) {
        if (data.hasOwnProperty(prefix)) {
            const plotable: any = {
                ...data[prefix],
                type: 'lines',
                name: 'line'
            }
            const layout = { fileopt: "overwrite", filename: "simple-node-example" };
            plotly.plot([plotable], layout, (err, msg) => {
                if (err) return console.log(err);
                console.log(msg);
            })
        }
    }*/
}
// fetchRawPlotRetro()

fetchRawPlot()
    .then((rawPlot) => {
        console.log(`RAW PLAOT = ${JSON.stringify(rawPlot)}`)
        process.exit()
        plotRawPlot(rawPlot)
    })
    /*.then(() => {
    return fetchRawPlotRetro()
})
.then((rawPlot) => {
    console.log(JSON.stringify(rawPlot))
    plotRawPlot(rawPlot)
})*/




/*const removeDuplicates: (rows: TypeRow[]) => Promise<TypeRow[]> = (rows) => {
    console.log(`I: REMOVEDUPLICATES, ROWS = ${ rows.length }`)
    const groups: TypeTransactGetItemsGroups = rows.reduce((transactGetItemsGroups: TypeTransactGetItemsGroups, row, index) => {
        const getItemsGroupIndex = transactGetItemsGroups.length - 1
        const getItemListIndex = transactGetItemsGroups[getItemsGroupIndex].length - 1
        transactGetItemsGroups[getItemsGroupIndex][getItemListIndex].push({
            Get: {
                Key: {
                    "pk": row.pk,
                    "sk": row.sk
                },
                TableName: tableName
            }
        })

        const currentGetItemsCount = transactGetItemsGroups[getItemsGroupIndex][getItemListIndex].length
        const lastIndex = rows.length - 1
        // console.log(`I: REMOVEDUPLICATES, lastIndex = ${ lastIndex } index = ${ index }`)
        if (((currentGetItemsCount % 25) == 0 || index >= lastIndex)) {
            const currentGroupCount = transactGetItemsGroups[getItemsGroupIndex].length
            if (index >= lastIndex) {
                //do nothing
            } else if (currentGroupCount >= MAX_SIMULATNEOUS_TRANSACT_GET_ITEMS_GROUPS) {
                transactGetItemsGroups.push([[]])
            } else {
                transactGetItemsGroups[getItemsGroupIndex].push([])
            }
        }
        return transactGetItemsGroups
    }, [[[]]])
    console.log(`I: REMOVEDUPLICATES, GROUPS = ${ groups.length }`)


    let responses: TransactGetItemsOutput[] = []
    return groups.reduce((promise, group) => {
        const promises = group.map((currentGetRequests) => {
            return new Promise((resolve, reject) => {
                ddb.transactGetItems({ TransactItems: currentGetRequests, ReturnConsumedCapacity: "TOTAL" }, (err, data) => {
                    if (data) {
                        updateCapacityUnits('TRANSACTGETITEMS-REMOVE_DUPLICATES', data.ConsumedCapacity)
                    }
                    if (err) {
                        console.log(`E: REMOVEDUPLICATES[FAILURE - GET GROUP]GROUP = ${ JSON.stringify(group) }`, err)
                        reject(err)
                    } else {
                        // console.log(`I: REMOVEDUPLICATES[SUCCESS - GET GROUP]GROUP = ${ JSON.stringify(group) }`)
                        console.log(`I: REMOVEDUPLICATES[SUCCESS - GET GROUP]data = ${ JSON.stringify(data) }`)
                        resolve(data)
                    }
                })
            })
        })
        return promise.then(() => Promise.all(promises)).then((responseshere: TransactGetItemsOutput[]) => {
            responses = responses.concat(responseshere)
            // console.log(`I: REMOVEDUPLICATES UPDATE RESPONSES RESPONSES = ${ responses.length } RESPONSES = ${ JSON.stringify(responses) }`)
            console.log(`I: REMOVEDUPLICATES UPDATE RESPONSES RESPONSES = ${ responses.length }`)
        })
    }, Promise.resolve())
        .then(() => {
            // console.log(`I: REMOVEDUPLICATES FINAL RESPONSES RESPONSES = ${ responses.length } RESPONSES = ${ JSON.stringify(responses) }`)
            console.log(`I: REMOVEDUPLICATES FINAL RESPONSES RESPONSES = ${ responses.length }`)
            // let count = 0
            // console.log("RESULTS, length: ", results.length)
            const newRows: TypeRow[] = responses.reduce((newRows: TypeRow[], result: TransactGetItemsOutput, index) => {
                const currentNewRows = result.Responses.reduce((currentNewRows: TypeRow[], currentResult, currentIndex) => {
                    // console.log("**********************************************************")
                    // console.log("RESULT.1>", JSON.stringify(currentResult))
                    // console.log("----------------------------------------------------------")
                    // console.log("ROW.2>", JSON.stringify(currentRow))
                    // console.log("**********************************************************")
                    // process.exit()

                    if (!currentResult.Item) {
                        //This means there was no entry
                        const currentRow = rows[index * 25 + currentIndex]
                        return [...currentNewRows, currentRow]
                        // console.log("THIS IS NOT ALREADY THERE: ADDING", JSON.stringify(currentResult))
                        // console.log("THIS IS NOT ALREADY THERE: ADDING", JSON.stringify(currentResult.Item))
                        // console.log("THIS IS NOT ALREADY THERE: ADDING", JSON.stringify(currentRow))
                    } else {
                        return currentNewRows
                        // count++
                        // console.log("THIS IS ALREADY THERE: SKIPPING", count)
                        // console.log("THIS IS ALREADY THERE: SKIPPING", JSON.stringify(currentResult))
                        // console.log("THIS IS ALREADY THERE: SKIPPING", JSON.stringify(currentRow))
                    }
                }, [])


                // console.log("**********************************************************")
                // console.log("newRows.3>", newRows.length)
                // console.log("newRows.3>", JSON.stringify(newRows))
                // console.log("**********************************************************")
                // process.exit()

                return [...newRows, ...currentNewRows]
            }, [])
            // console.log("**********************************************************")
            // console.log("oldRows.4>", rows.length)
            // console.log("newRows.4>", newRows.length)
            // console.log(`newRows.4 > ${ JSON.stringify(newRows) }`)
            // console.log("**********************************************************")
            // process.exit()
            // console.log(`I: REMOVEDUPLICATES FINAL NEWROWS NEWROWS = ${ newRows.length } RESPONSES = ${ JSON.stringify(newRows) }`)
            console.log(`I: REMOVEDUPLICATES FINAL NEWROWS NEWROWS = ${ newRows.length }`)
            return newRows
        })
    // process.exit()
}*/